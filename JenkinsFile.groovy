#!groovy

/*def remote = [:]
remote.name = "ec2-3-87-205-14.compute-1.amazonaws.com"
remote.host = "ec2-3-87-205-14.compute-1.amazonaws.com"
remote.user = "ubuntu"
remote.identityFile = "/var/lib/jenkins/.ssh/server-app-key"
remote.allowAnyHosts = true*/ //RDffzv8oAZE9seWHozqe

//this is a new comment

pipeline {
    agent any
    
    options {
      gitLabConnection('test_repo')
    }
    
    triggers {
        gitlab(triggerOnPush: true, 
               triggerOnMergeRequest: false, 
               branchFilterType: 'NameBasedFilter',
               includeBranchesSpec: "master",
               excludeBranchesSpec: "",)
    }

    stages {
        
        stage("Prepare") {
            steps {
                sh 'echo "Hello World, Im preparing"'
                sh 'sudo bash /opt/jboss/jboss-eap-7.1/bin/jboss-cli.sh --connect --command="undeploy myApp.war"'
                //sh 'sudo bash /opt/jboss/jboss-eap-7.1/bin/jboss-cli.sh --connect --command="shutdown"'
            }
        }

        stage('Download from repository') {
            steps {
                sh 'echo "Hello World, Im pulling"'
                checkout scm
            }
        }

        stage('Build') {
            steps {
                 sh 'echo "Hello World, Im building"'
            }
        }

        stage('Upload artifacts') {
            steps {
                 sh 'echo "Hello World, Im uploading"'
            }
        }

        stage('Deploy') {
            steps {
                 sh 'echo "Hello World, Im deploying"'
                 //  sh 'sudo bash /opt/jboss/jboss-eap-7.1/bin/standalone.sh'
                 sh 'sudo bash /opt/jboss/jboss-eap-7.1/bin/jboss-cli.sh --connect --command="deploy --force /home/ubuntu/myApp.war"'
            }
        }

        stage('Test') {
            steps {
                 sh 'echo "Hello World, Im testing"'
            }
        }
    }
}