package com.example.jsfintroduction;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@Named
@RequestScoped
public class WelcomePageBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String welcomeUserName;
	private String completedGreeting;
	
	public void sayHello() {
		completedGreeting = "Hello, " + welcomeUserName;
	}
	
	public String getWelcomeUserName() {
		return welcomeUserName;
	}
	
	public void setWelcomeUserName(String welcomeUserName) {
		this.welcomeUserName = welcomeUserName;
	}
	
	public String getCompletedGreeting() {
		return completedGreeting;
	}
	
	public void setCompletedGreeting(String completedGreeting) {
		this.completedGreeting = completedGreeting;
	}
	
	

}
